#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

'''
Created on 06.04.2015.

@author: Aleksandra Mitrovic
'''
from PySide import QtGui, QtCore


class Graph(QtGui.QDialog):
    
    def __init__(self, parent = None):
        """
        Konstruktor grafika.
        
        Args:
            parent: (QWidget) roditelj za dati widget.
        """
        QtGui.QDialog.__init__(self, parent)
        self.setMinimumSize(640, 480)
        self.setWindowTitle("Graph")
        self.setWindowIcon(QtGui.QIcon("resources/graph.ico"))
        
        self._data = []
        self._time = []
        
        self._layout = QtGui.QGridLayout()
        self._h_layout = QtGui.QHBoxLayout()
        self.setLayout(self._layout)
        
        self.polyline = QtGui.QPainterPath()
        
        self.scene = QtGui.QGraphicsScene(self)
        self.scene.addPath(self.polyline)
        self.view = QtGui.QGraphicsView(self)
        self.view.setScene(self.scene)
        self.view.scale(1, -1)
        
        self.check_1 = QtGui.QCheckBox("Ball 1", self)
        self.check_2 = QtGui.QCheckBox("Ball 2", self)
        self.check_3 = QtGui.QCheckBox("Ball 3", self)
        self.check_4 = QtGui.QCheckBox("Ball 4", self)
        self.check_5 = QtGui.QCheckBox("Ball 5", self)
        self.check_6 = QtGui.QCheckBox("Ball 6", self)
        self.check_7 = QtGui.QCheckBox("Ball 7", self)
        self.check_8 = QtGui.QCheckBox("Ball 8", self)
        self.check_9 = QtGui.QCheckBox("Ball 9", self)
        self.check_10 = QtGui.QCheckBox("Ball 10", self)
        self.check_11 = QtGui.QCheckBox("Ball 11", self)
        
        self.check_1.setChecked(True)
        self.check_2.setChecked(True)
        self.check_3.setChecked(True)
        self.check_4.setChecked(True)
        self.check_5.setChecked(True)
        self.check_6.setChecked(True)
        self.check_7.setChecked(True)
        self.check_8.setChecked(True)
        self.check_9.setChecked(True)
        self.check_10.setChecked(True)
        self.check_11.setChecked(True)
        
        self._ch_data = [True, True, True, True, True, True, True, True, True, True, True]
        
        self.check_1.stateChanged.connect(lambda: self._on_check_any(self.check_1, 0))
        self.check_2.stateChanged.connect(lambda: self._on_check_any(self.check_2, 1))
        self.check_3.stateChanged.connect(lambda: self._on_check_any(self.check_3, 2))
        self.check_4.stateChanged.connect(lambda: self._on_check_any(self.check_4, 3))
        self.check_5.stateChanged.connect(lambda: self._on_check_any(self.check_5, 4))
        self.check_6.stateChanged.connect(lambda: self._on_check_any(self.check_6, 5))
        self.check_7.stateChanged.connect(lambda: self._on_check_any(self.check_7, 6))
        self.check_8.stateChanged.connect(lambda: self._on_check_any(self.check_8, 7))
        self.check_9.stateChanged.connect(lambda: self._on_check_any(self.check_9, 8))
        self.check_10.stateChanged.connect(lambda: self._on_check_any(self.check_10, 9))
        self.check_11.stateChanged.connect(lambda: self._on_check_any(self.check_11, 10))
        
        self._h_layout.addWidget(self.check_1)
        self._h_layout.addWidget(self.check_2)
        self._h_layout.addWidget(self.check_3)
        self._h_layout.addWidget(self.check_4)
        self._h_layout.addWidget(self.check_5)
        self._h_layout.addWidget(self.check_6)
        self._h_layout.addWidget(self.check_7)
        self._h_layout.addWidget(self.check_8)
        self._h_layout.addWidget(self.check_9)
        self._h_layout.addWidget(self.check_10)
        self._h_layout.addWidget(self.check_11)
        
        self._layout.addLayout(self._h_layout, 0, 0)
        self._layout.addWidget(self.view, 1, 0)

    def _on_check_any(self, widget, index):
        """
        Metoda koja obradjuje dogadjaj klika na checkbox.
        
        Args:
            widget: (QCheckBox) checkbox.
            index: (int) indeks statusnog polja za dati checkbox.
        """
        self._ch_data[index] = widget.checkState()
        self.draw_graph()

    def set_data(self, time, data):
        """
        Metoda koja postavlja nove vrednosti za polja data i time.
        
        Args:
            time: (list) lista vremenskih odsecaka.
            data: (list) lista koja sadrzi podatke o brzinama svih lopti.
        """
        self._data = data
        self._time = time
        
        self.draw_graph()
        
    def draw_graph(self):
        """
        Metoda koja crta grafik.
        """
        
        self.scene.clear()
        self.polyline = [QtGui.QPainterPath(), QtGui.QPainterPath(), QtGui.QPainterPath(), QtGui.QPainterPath(), QtGui.QPainterPath(),
                         QtGui.QPainterPath(), QtGui.QPainterPath(), QtGui.QPainterPath(), QtGui.QPainterPath(), QtGui.QPainterPath(),
                         QtGui.QPainterPath()]
        
        grey_pen = QtGui.QPen(QtGui.QColor(190, 190, 190))
        
        grid_pen = QtGui.QPen(QtGui.QColor(200, 200, 200))
        grid_pen.setStyle(QtCore.Qt.DotLine)
        
        self.scene.setSceneRect(-50, -25, (len(self._time) + 20)*10, 575)
        
        line_pen = [QtGui.QPen(QtGui.QColor(0, 0, 255)), QtGui.QPen(QtGui.QColor(160, 65, 197)),
                    QtGui.QPen(QtGui.QColor(0, 0, 255)), QtGui.QPen(QtGui.QColor(255, 0, 0)),
                    QtGui.QPen(QtGui.QColor(255, 122, 0)), QtGui.QPen(QtGui.QColor(0, 0, 255)),
                    QtGui.QPen(QtGui.QColor(0, 0, 255)), QtGui.QPen(QtGui.QColor(0, 0, 255)),
                    QtGui.QPen(QtGui.QColor(0, 0, 255)), QtGui.QPen(QtGui.QColor(0, 0, 255)), QtGui.QPen(QtGui.QColor(0, 0, 255))]
        
        #line_pen[0].setStyle(QtCore.Qt.DashLine)
        
        #Iscrtaj x osu
        self.scene.addLine(-10, 0, (len(self._time) + 1)*10, 0, grey_pen)
        t_label = QtGui.QGraphicsSimpleTextItem("t")
        t_label.setPos((len(self._time) + 1) * 10, -5)
        t_label.setBrush(QtGui.QBrush(QtGui.QColor(190, 190, 190)))
        t_label.scale(1, -1)
        self.scene.addItem(t_label)
        
        #Strelica
        self.scene.addLine((len(self._time) + 1)*10 - 5, 3, (len(self._time) + 1)*10, 0, grey_pen)
        self.scene.addLine((len(self._time) + 1)*10 - 5, -3, (len(self._time) + 1)*10, 0, grey_pen)
        
        #Iscrtaj y osu
        self.scene.addLine(0, -10, 0, 530, grey_pen)
        v_label = QtGui.QGraphicsSimpleTextItem("v")
        v_label.setPos(-11, 530)
        v_label.setBrush(QtGui.QBrush(QtGui.QColor(190, 190, 190)))
        v_label.scale(1, -1)
        self.scene.addItem(v_label)
        
        #Strelica
        self.scene.addLine(-3, 525, 0, 530, grey_pen)
        self.scene.addLine(0, 530, 3, 525, grey_pen)
        
        #Iscrtaj podeoke za brzinu
        for y in range(1, 52):
            self.scene.addLine(3, y*10, (len(self._time))*10, y*10, grid_pen)
            self.scene.addLine(-3, y*10, 3, y*10, grey_pen)
        
        #Iscrtaj podeoke za vreme
        for x in range(1, len(self._time)):
            self.scene.addLine(x*10, 3, x*10, 520, grid_pen)
            self.scene.addLine(x*10, 3, x*10, -3, grey_pen)
            
        
        for i in range(len(self._data)):
            if self._ch_data[i]:
                for j in range(len(self._data[i])):
                    self.polyline[i].lineTo(self._time[j]*1000, self._data[i][j]*100)
            self.scene.addPath(self.polyline[i], line_pen[0])
        
        '''    
        #Iscrtaj legendu
        self.scene.addRect((len(self._time) + 2) * 10, 530 - 115, 90, 115, grey_pen)
        #Lopte
        self.scene.addRect((len(self._time) + 2) * 10 + 5, 530 - 15, 20, 10, grey_pen, QtGui.QBrush(QtGui.QColor(0, 0, 0)))
        
        ball1_label = QtGui.QGraphicsSimpleTextItem("Ball 1")
        ball1_label.setPos((len(self._time) + 2) * 10 + 30, 530 - 4)
        ball1_label.setBrush(QtGui.QBrush(QtGui.QColor(190, 190, 190)))
        ball1_label.scale(1, -1)
        self.scene.addItem(ball1_label)
        
        self.scene.addRect((len(self._time) + 2) * 10 + 5, 530 - 30, 20, 10, grey_pen, QtGui.QBrush(QtGui.QColor(160, 65, 197)))
        
        ball2_label = QtGui.QGraphicsSimpleTextItem("Ball 2")
        ball2_label.setPos((len(self._time) + 2) * 10 + 30, 530 - 19)
        ball2_label.setBrush(QtGui.QBrush(QtGui.QColor(190, 190, 190)))
        ball2_label.scale(1, -1)
        self.scene.addItem(ball2_label)
        
        self.scene.addRect((len(self._time) + 2) * 10 + 5, 530 - 45, 20, 10, grey_pen, QtGui.QBrush(QtGui.QColor(0, 0, 255)))
        
        ball3_label = QtGui.QGraphicsSimpleTextItem("Ball 3")
        ball3_label.setPos((len(self._time) + 2) * 10 + 30, 530 - 34)
        ball3_label.setBrush(QtGui.QBrush(QtGui.QColor(190, 190, 190)))
        ball3_label.scale(1, -1)
        self.scene.addItem(ball3_label)
        
        self.scene.addRect((len(self._time) + 2) * 10 + 5, 530 - 60, 20, 10, grey_pen, QtGui.QBrush(QtGui.QColor(255, 0, 0)))
        
        ball4_label = QtGui.QGraphicsSimpleTextItem("Ball 4")
        ball4_label.setPos((len(self._time) + 2) * 10 + 30, 530 - 49)
        ball4_label.setBrush(QtGui.QBrush(QtGui.QColor(190, 190, 190)))
        ball4_label.scale(1, -1)
        self.scene.addItem(ball4_label)
        
        self.scene.addRect((len(self._time) + 2) * 10 + 5, 530 - 75, 20, 10, grey_pen, QtGui.QBrush(QtGui.QColor(255, 122, 0)))
        
        ball5_label = QtGui.QGraphicsSimpleTextItem("Ball 5")
        ball5_label.setPos((len(self._time) + 2) * 10 + 30, 530 - 64)
        ball5_label.setBrush(QtGui.QBrush(QtGui.QColor(190, 190, 190)))
        ball5_label.scale(1, -1)
        self.scene.addItem(ball5_label)
        
        #Duzine podeoka
        
        self.scene.addLine((len(self._time) + 2) * 10 + 5, 530 - 87, (len(self._time) + 2) * 10 + 5, 530 - 93, grey_pen)
        self.scene.addLine((len(self._time) + 3) * 10 + 5, 530 - 87, (len(self._time) + 3) * 10 + 5, 530 - 93, grey_pen)
        self.scene.addLine((len(self._time) + 2) * 10 + 5, 530 - 90, (len(self._time) + 3) * 10 + 5, 530 - 90, grey_pen)
        
        dt_label = QtGui.QGraphicsSimpleTextItem("t = 0.01s")
        dt_label.setPos((len(self._time) + 2) * 10 + 30, 530 - 83)
        dt_label.setBrush(QtGui.QBrush(QtGui.QColor(190, 190, 190)))
        dt_label.scale(1, -1)
        self.scene.addItem(dt_label)
        
        
        self.scene.addLine((len(self._time) + 2) * 10 + 5, 530 - 102, (len(self._time) + 2) * 10 + 5, 530 - 108, grey_pen)
        self.scene.addLine((len(self._time) + 3) * 10 + 5, 530 - 102, (len(self._time) + 3) * 10 + 5, 530 - 108, grey_pen)
        self.scene.addLine((len(self._time) + 2) * 10 + 5, 530 - 105, (len(self._time) + 3) * 10 + 5, 530 - 105, grey_pen)
        
        dt_label = QtGui.QGraphicsSimpleTextItem("v = 0.1m/s")
        dt_label.setPos((len(self._time) + 2) * 10 + 30, 530 - 98)
        dt_label.setBrush(QtGui.QBrush(QtGui.QColor(190, 190, 190)))
        dt_label.scale(1, -1)
        self.scene.addItem(dt_label)'''
        