#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

'''
Created on 15.02.2015.

@author: Ivan Radosavljevic
         Aleksandra Mitrovic
'''

from PySide import QtGui, QtCore
from core.gl_widget import GlWidget
from math import sin, cos, radians
from core.world import IntegrationType
from core.graph import Graph

class Gui(QtGui.QMainWindow):

    def __init__(self):
        """
        Konstruktor klase Gui
        """
        QtGui.QMainWindow.__init__(self)
        
        self.setMinimumSize(640, 480)
        self.setWindowTitle("Billiards")
        self.setWindowIcon(QtGui.QIcon("resources/icon.ico"))
        
        self.dock_widget = DockWidget(self)
        self.gl = GlWidget(self)
        
        self.dock_widget.set_world(self.gl.world)
        
        self.setCentralWidget(self.gl)
        self.dw = QtGui.QDockWidget("Controls", self)
        self.dw.setAllowedAreas(QtCore.Qt.RightDockWidgetArea | QtCore.Qt.LeftDockWidgetArea)
        self.dw.setWidget(self.dock_widget)
        self.dw.setFeatures(QtGui.QDockWidget.DockWidgetMovable | QtGui.QDockWidget.DockWidgetFloatable)
        
        self.addDockWidget(QtCore.Qt.RightDockWidgetArea, self.dw)
        
 


class DockWidget(QtGui.QWidget):
    def __init__(self, parent = None, world = None):
        """
        Konstruktor klase DockWidget.
        
        Args:
            parent: (QWidget) roditelj DockWidget-a.
            world: (Object) model sveta u kom se vrsi simulacija.
        """
        QtGui.QWidget.__init__(self, parent)
        
        self._parent = parent
        
        self.setSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Fixed)
        self.layout = QtGui.QGridLayout(self)
        
        self.x = 0
        self.y = 0
        self.force = 0
        self.direction = [0, 0, 0]
        
        self._world = world
        
        self.selected_row = -1
        
        self.methods_label = QtGui.QLabel("Method", self)
        self.methods_list = QtGui.QComboBox(self)
        
        self.x_label = QtGui.QLabel("Direction", self)
        self.f_label = QtGui.QLabel("Force", self)
        
        self.x_slider = QtGui.QSlider(QtCore.Qt.Horizontal, self)
        self.f_slider = QtGui.QSlider(QtCore.Qt.Horizontal, self)
        
        self.x_spin_box = QtGui.QSpinBox(self)
        self.f_spin_box = QtGui.QDoubleSpinBox(self)
        
        self.cr_label = QtGui.QLabel("Coefficient of restitution")
        self.cr_slider = QtGui.QSlider(QtCore.Qt.Horizontal, self)
        self.cr_spin_box = QtGui.QDoubleSpinBox(self)
        
        self.materials = {"Steel" : 0.87 , "Wood" : 0.75, "Concrete" : 0.81, "Cloth" : 0.50}
        self.material_label = QtGui.QLabel("Material", self)
        self.material_list = QtGui.QComboBox(self)
        
        self.table_widget = QtGui.QTableWidget(0, 3, self)
        
        self.graph_button = QtGui.QPushButton("Show graph", self)
        
        self.push_button = QtGui.QPushButton("Hit", self)
        
        
        methods= ["Euler", "RK4"]
        self.methods_list.addItems(methods)
        
        self._conf_slider(self.x_slider, 360, 20, 5, 10)
        self._conf_slider(self.f_slider, 250, 10, 5, 5)
        
        self.x_slider.valueChanged.connect(self._on_x_slider)
        self.f_slider.valueChanged.connect(self._on_f_slider)
        
        self.x_spin_box.setMaximum(360)
        self.x_spin_box.setSuffix("\u00B0")
        self.f_spin_box.setMaximum(250)
        self.f_spin_box.setSuffix("N")
        
        self.f_spin_box.setSingleStep(0.1)
        
        self.x_spin_box.valueChanged.connect(self._on_x_spin_box)
        self.f_spin_box.valueChanged.connect(self._on_f_spin_box)
        
        self._conf_slider(self.cr_slider, 100, 2, 10, 2)
        self.cr_slider.valueChanged.connect(self._on_cr_slider)
        
        self.cr_spin_box.setMaximum(1)
        self.cr_spin_box.setMinimum(0)
        self.cr_spin_box.valueChanged.connect(self._on_cr_spin_box)
        self.cr_spin_box.setValue(0.5)
        self.cr_spin_box.setSingleStep(0.05)
        
        
        lista = list(self.materials.keys())
        lista.sort()
        self.material_list.addItems(lista)
        self.material_list.addItem("Custom")
        self.material_list.currentIndexChanged.connect(self._on_material_list)
        
        self.table_widget.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)
        self.table_widget.setSelectionBehavior(QtGui.QAbstractItemView.SelectRows)
        self.table_widget.setSelectionMode(QtGui.QAbstractItemView.SingleSelection)
        self.table_widget.verticalHeader().hide()
        
        self.table_widget.setHorizontalHeaderItem(0, QtGui.QTableWidgetItem("Ball"))
        self.table_widget.setHorizontalHeaderItem(1, QtGui.QTableWidgetItem("Direction"))
        self.table_widget.setHorizontalHeaderItem(2, QtGui.QTableWidgetItem("Velocity"))
        
        self.table_widget.setColumnWidth(0, 70)
        self.table_widget.setColumnWidth(1, 70)
        self.table_widget.horizontalHeader().setStretchLastSection(True)
        
        
        self.layout.addWidget(self.methods_label, 1, 0)
        self.layout.addWidget(self.methods_list, 2, 0, 1, 2)
        self.layout.addWidget(self.x_label, 3, 0)
        self.layout.addWidget(self.x_slider, 4, 0)
        self.layout.addWidget(self.f_label, 5, 0)
        self.layout.addWidget(self.f_slider, 6, 0)
        self.layout.addWidget(self.cr_label, 7, 0)
        self.layout.addWidget(self.cr_slider, 8, 0)
        
        self.layout.addWidget(self.x_spin_box, 4, 1)
        self.layout.addWidget(self.f_spin_box, 6, 1)
        self.layout.addWidget(self.cr_spin_box, 8, 1)
        
        self.layout.addWidget(self.material_label, 9, 0)
        self.layout.addWidget(self.material_list, 10, 0, 1, 2)
        
        self.layout.addWidget(self.table_widget, 11, 0, 1, 2)
        
        self.layout.addWidget(self.graph_button, 12, 0, 1, 2)
        
        self.layout.addWidget(self.push_button, 13, 0, 1, 2)
        
        self.graph_button.clicked.connect(self._on_show_graph_button)
        self.push_button.clicked.connect(self._on_push_button)
    
    def set_world(self, world):
        """
        Metoda koja dodeljuje polju world objekat modela simulacije sveta.
        
        Args:
            world: (Object) model simulacije sveta.
        """
        self._world = world
    
    def _add_row(self, ball, x, velocity):
        """
        Metoda koja dodaje novi red u tabelu i postavlja inicijalne vrednosti u dati red

        Args:
            ball: (string) naziv lopte
            x: (int) vrednost sa x koordinate
            velocity: (float) brzina
        """
        _ball = QtGui.QTableWidgetItem(ball)
        _x = QtGui.QTableWidgetItem(str(x))
        _velocity = QtGui.QTableWidgetItem(velocity)
        
        _ball.setTextAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
        _x.setTextAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        _velocity.setTextAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        
        self.table_widget.insertRow(self.table_widget.rowCount())
        self.table_widget.setItem(self.table_widget.rowCount() - 1, 0, _ball)
        self.table_widget.setItem(self.table_widget.rowCount() - 1, 1, _x)
        self.table_widget.setItem(self.table_widget.rowCount() - 1, 2, _velocity)
        
    def _modify_row(self, row, ball, x, velocity):
        """
        Metoda koja postavlja nove vrednosti za dati red u tabeli

        Args:
            ball: (string) naziv lopte
            x: (int) vrednost sa x koordinate
            y: (int) vrednost sa y koordinate
            velocity: (float) brzina
        """
        _ball = QtGui.QTableWidgetItem(ball)
        _x = QtGui.QTableWidgetItem(str(x))
        _velocity = QtGui.QTableWidgetItem(velocity)
        
        _ball.setTextAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
        _x.setTextAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        _velocity.setTextAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        
        self.table_widget.setItem(row, 0, _ball)
        self.table_widget.setItem(row, 1, _x)
        self.table_widget.setItem(row, 2, _velocity)
        
    def _row_selected(self):
        pass
    def _conf_slider(self, widget, max_value, single_step, page_step, tick_interval):
        """
        Metoda koja podesava slajder.
        
        Args:
            widget: (QWidget) slajder koji se zeli podesavati
            max_value: (int) maksimalna vrednost do koje ce slajder ici
            single_step: (int) pomeraj pritiskom na strelice
            page_step: (int) pomeraj pritiskom na page up ili down
            tick_interval: (int) rastojanje izmedju crtica kod slajdera
        """
        widget.setRange(0, max_value)
        widget.setSingleStep(single_step)
        widget.setPageStep(page_step)
        widget.setTickInterval(tick_interval)
        widget.setTickPosition(QtGui.QSlider.TicksBelow)
        
    def _on_x_spin_box(self):
        """
        Metoda koja postavlja vrednost sa x spin_box-a na slider
        
        """
        self.x_slider.setValue(self.x_spin_box.value())
        
    def _on_f_spin_box(self):
        """
        Metoda koja postavlja vrednost sa f spin_box-a na slider
        
        """
        self.f_slider.setValue(self.f_spin_box.value())
        
    def _on_cr_spin_box(self):
        """
        Metoda koja postavlja vrednost sa cr spin_box-a na slider
        """
        self.cr_slider.setValue(self.cr_spin_box.value()*100)
        lista = list(self.materials.keys())
        lista.sort()
        
        for i in range(len(lista)):
            if self.materials[lista[i]] == self.cr_spin_box.value():
                self.material_list.setCurrentIndex(i)
                break
            else:
                self.material_list.setCurrentIndex(self.material_list.count()-1)
        
    def _on_x_slider(self):
        """
        Metoda koja postavlja vrednost sa x slider-a na spin_box
        
        """
        self.x_spin_box.setValue(self.x_slider.value())
        
    def _on_f_slider(self):
        """
        Metoda koja postavlja vrednost sa f slider-a na spin_box
        
        """
        self.f_spin_box.setValue(self.f_slider.value())
        
    def _on_cr_slider(self):
        """
        Metoda koja postavlja vrednost sa f slider-a na spin_box
        """
        self.cr_spin_box.setValue(self.cr_slider.value()/100)
    
    def _on_material_list(self):
        """
        Metoda koja podesava koeficijent restitucije na slider-u i spin_box-u.
        """
        txt = self.material_list.currentText()
        if txt != "Custom":
            self.cr_spin_box.setValue(self.materials[txt])
    
    def _on_show_graph_button(self):
        """
        Metoda koja obradjuje dogadjak klika na Show graph button.
        """
        graph = Graph(self._parent)
        graph.set_data(self._world.time, (self._world._objects[0]._graph_data, self._world._objects[1]._graph_data,
                                            self._world._objects[2]._graph_data, self._world._objects[3]._graph_data,
                                            self._world._objects[4]._graph_data, self._world._objects[5]._graph_data,
                                            self._world._objects[6]._graph_data, self._world._objects[7]._graph_data,
                                            self._world._objects[8]._graph_data, self._world._objects[9]._graph_data,
                                            self._world._objects[10]._graph_data))
        
        graph.exec_()
     
    def _on_push_button(self):
        """
        Metoda koja u promenljive smesta iscitane vrednosti sa widget-a.
        """
        self.selected_row = self.table_widget.currentRow()
        
        self._world.time = []
        
        if self.methods_list.currentText() == "Euler":
            self._world.set_integration_type(IntegrationType.EULER)
        elif self.methods_list.currentText() == "RK4":
            self._world.set_integration_type(IntegrationType.RK4)
            
        self._world._objects[-1].set_cr(self.cr_spin_box.value())
        
        
        for i in range(len(self._world._objects)-7):
            self._world._objects[i].reset_graph_data()
        
        self._world._objects[0]._force = self.f_spin_box.value()
        self._world._objects[0]._direction[0] = (-1)*cos(radians(self.x_spin_box.value()))
        self._world._objects[0]._direction[2] = (1)*sin(radians(self.x_spin_box.value()))
        