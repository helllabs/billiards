#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

'''
Created on 15.02.2015.

@author: Ivan Radosavljevic
         Aleksandra Mitrovic
'''
import time
from PySide import QtOpenGL, QtGui
from OpenGL import GL
from math import degrees
from core.mesh import Mesh
from core.world import World
from core.ball import Ball
from core.table import Table
from core.goal import Goal
from math import atan2, sqrt


class GlWidget(QtOpenGL.QGLWidget):
    
    def __init__(self, parent = None):
       
        QtOpenGL.QGLWidget.__init__(self, parent)
        self._parent = parent
        
        self._arrow = Mesh(self, "resources/strelica.obj", "resources/strelica.png")
        self._arrow.set_position(0, -0.36, 0)
        self._arrow.y_rot = 1
        
        self.world = World(self)
        
        self._initWorld()
        
        self.trolltechPurple = QtGui.QColor.fromCmykF(0.12, 0.12, 0.0, 0.0)
    
    def _initWorld(self):
        
        self.world.add_object(Ball(self.world, Mesh(self, "resources/kugla.obj", "resources/kugla0.png")))
        self.world.add_object(Ball(self.world, Mesh(self, "resources/kugla.obj", "resources/kugla1.png")))
        self.world.add_object(Ball(self.world, Mesh(self, "resources/kugla.obj", "resources/kugla2.png")))
        self.world.add_object(Ball(self.world, Mesh(self, "resources/kugla.obj", "resources/kugla3.png")))
        self.world.add_object(Ball(self.world, Mesh(self, "resources/kugla.obj", "resources/kugla4.png")))
        self.world.add_object(Ball(self.world, Mesh(self, "resources/kugla.obj", "resources/kugla5.png")))
        self.world.add_object(Ball(self.world, Mesh(self, "resources/kugla.obj", "resources/kugla6.png")))
        self.world.add_object(Ball(self.world, Mesh(self, "resources/kugla.obj", "resources/kugla7.png")))
        self.world.add_object(Ball(self.world, Mesh(self, "resources/kugla.obj", "resources/kugla8.png")))
        self.world.add_object(Ball(self.world, Mesh(self, "resources/kugla.obj", "resources/kugla9.png")))
        self.world.add_object(Ball(self.world, Mesh(self, "resources/kugla.obj", "resources/kugla10.png")))
        
        #dodavanje rupa
        self.world.add_object(Goal(self.world, None, [-1.25, -0.36, -2.53]))
        self.world.add_object(Goal(self.world, None, [1.25, -0.36, -2.53]))
        
        self.world.add_object(Goal(self.world, None, [-1.28, -0.36, 0]))
        self.world.add_object(Goal(self.world, None, [1.28, -0.36, 0]))
        
        self.world.add_object(Goal(self.world, None, [-1.25, -0.36, 2.53]))
        self.world.add_object(Goal(self.world, None, [1.25, -0.36, 2.53]))
        
        self.world.add_object(Table(self.world, Mesh(self, "resources/bilijar.obj", "resources/sto.png")))
        
        #postavljanje kugli
        
        self.world._objects[0].set_position([0, -0.36, 0.0])
        
        self.world._objects[1].set_position([0.0, -0.36, -0.50])
        
        self.world._objects[2].set_position([0.06, -0.36, -0.6])
        self.world._objects[3].set_position([-0.06, -0.36, -0.6])
        
        self.world._objects[4].set_position([0.12, -0.36, -0.7])
        self.world._objects[5].set_position([-0.12, -0.36, -0.7])
        
        self.world._objects[6].set_position([0.18, -0.36, -0.8])
        self.world._objects[7].set_position([0.06, -0.36, -0.8])
        self.world._objects[8].set_position([-0.06, -0.36, -0.8])
        self.world._objects[9].set_position([-0.18, -0.36, -0.8])
        
        self.world._objects[10].set_position([0.0, -0.36, -0.7])
        
        #dodavanje objekata u tabelu
        for i in range(len(self.world._objects)-7):
                self._parent.dock_widget._add_row("Ball {0}".format(i), \
                                                  abs(round(degrees(atan2(self.world._objects[i]._velocity[2], self.world._objects[i]._velocity[0])) - 180, 0)), 0)
    
    def reset_balls(self):
        for i in range(len(self.world._objects) - 7):
            self.world._objects[i]._velocity = [0, 0, 0]
            
        self.world._objects[0].set_position([0, -0.36, 0.0])
        
        self.world._objects[1].set_position([0.0, -0.36, -0.50])
        
        self.world._objects[2].set_position([0.06, -0.36, -0.6])
        self.world._objects[3].set_position([-0.06, -0.36, -0.6])
        
        self.world._objects[4].set_position([0.12, -0.36, -0.7])
        self.world._objects[5].set_position([-0.12, -0.36, -0.7])
        
        self.world._objects[6].set_position([0.18, -0.36, -0.8])
        self.world._objects[7].set_position([0.06, -0.36, -0.8])
        self.world._objects[8].set_position([-0.06, -0.36, -0.8])
        self.world._objects[9].set_position([-0.18, -0.36, -0.8])
        
        self.world._objects[10].set_position([0.0, -0.36, -0.7])
    
    def initializeGL(self):
        self.qglClearColor(self.trolltechPurple.darker())

        GL.glLight(GL.GL_LIGHT0, GL.GL_POSITION, (-5, -10, 50))
        GL.glLight(GL.GL_LIGHT0, GL.GL_DIFFUSE, (1.00, 1.00, 1.00))

        GL.glFrontFace(GL.GL_CW)
        GL.glShadeModel(GL.GL_FLAT)
        GL.glEnable(GL.GL_DEPTH_TEST)
        GL.glEnable(GL.GL_LIGHTING)
        GL.glEnable(GL.GL_LIGHT0)
        GL.glEnable(GL.GL_CULL_FACE)
        GL.glEnable(GL.GL_TEXTURE_2D)
        GL.glEnable(GL.GL_BLEND)

    def paintGL(self):
        start = time.time()
        GL.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT)
        GL.glLoadIdentity()
        
        GL.glTranslated(0.0, 0.0, -11.0)
        GL.glRotated(-90, 1.0, 0.0, 0.0)
        GL.glRotated(0, 0.0, 1.0, 0.0)
        GL.glRotated(0, 0.0, 0.0, 1.0)
        
        self.world.update()
        
        for i in range(len(self.world._objects)-7):
            self._parent.dock_widget._modify_row(i, "Ball {0}".format(i+1), \
                                                  abs(round(degrees(atan2(self.world._objects[i]._velocity[2], self.world._objects[i]._velocity[0])) - 180, 0)), \
                                                  "{0:.2f}".format(sqrt(self.world._objects[i]._velocity[0]**2 + self.world._objects[i]._velocity[2]**2)))
        
        if self.world._simulating == False:
            self._arrow.set_position(self.world._objects[0]._bounds._position[0], -0.36,
                                        self.world._objects[0]._bounds._position[2])
            self._arrow.angle = self._parent.dock_widget.x_spin_box.value()
            self._arrow.draw()
            
            self._parent.dock_widget.methods_list.setEnabled(True)
            self._parent.dock_widget.x_slider.setEnabled(True)
            self._parent.dock_widget.x_spin_box.setEnabled(True)
            self._parent.dock_widget.f_slider.setEnabled(True)
            self._parent.dock_widget.f_spin_box.setEnabled(True)
            self._parent.dock_widget.cr_slider.setEnabled(True)
            self._parent.dock_widget.cr_spin_box.setEnabled(True)
            self._parent.dock_widget.material_list.setEnabled(True)
            self._parent.dock_widget.graph_button.setEnabled(True)
            self._parent.dock_widget.push_button.setEnabled(True)
        else:
            self._parent.dock_widget.methods_list.setEnabled(False)
            self._parent.dock_widget.x_slider.setEnabled(False)
            self._parent.dock_widget.x_spin_box.setEnabled(False)
            self._parent.dock_widget.f_slider.setEnabled(False)
            self._parent.dock_widget.f_spin_box.setEnabled(False)
            self._parent.dock_widget.cr_slider.setEnabled(False)
            self._parent.dock_widget.cr_spin_box.setEnabled(False)
            self._parent.dock_widget.material_list.setEnabled(False)
            self._parent.dock_widget.graph_button.setEnabled(False)
            self._parent.dock_widget.push_button.setEnabled(False)
            
        
        end = time.time()
        
        if (end - start) < 0.016:
            time.sleep(0.016 - (end - start))
        #end = time.time()
        #print(end - start)
        self.update()

    def resizeGL(self, width, height):
        side = min(width, height)
        GL.glViewport(int((width - side) / 2), int((height - side) / 2), side, side)

        GL.glMatrixMode(GL.GL_PROJECTION)
        GL.glLoadIdentity()
        GL.glFrustum(-1, 1, 1, -1, 4.0, 20.0)
        GL.glMatrixMode(GL.GL_MODELVIEW)