#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

'''
Created on 20.02.2015.

@author: Ivan Radosavljevic
         Aleksandra Mitrovic
'''
from math import sqrt


class BoundingSphere:
    def __init__(self, position, r):
        """
        Konstruktor objekta tipa BoundingSphere.
        
        Args:
            position: (list) lista koja opisuje poziciju sfere preko x, y i z koordinate.
            r: (float) poluprecnik sfere.
        """
        self._position = position
        self._r = r
    
    def x(self):
        """
        Metoda koja vraca x koordinatu pozicije.
        
        Returns:
            X koordinata.
        """
        return self._position[0]
    
    def y(self):
        """
        Metoda koja vraca y koordinatu pozicije.
        
        Retruns:
            Y kooridanta.
        """
        return self._position[1]
    
    def z(self):
        """
        Metoda koja vraca z koordinatu pozicije.
        
        Returns:
            Z koordinata.
        """
        return self._position[2]
    
    def set_position(self, position):
        """
        Metoda koja postavlja novu poziciju sfere.
        
        Args:
            position: (list) lista sa x, y i z koordinatama pozicije.
        """
        self._position = position
        
    def check_collision(self, obj):
        """
        Metoda koja proverava da li je doslo do kolizije sa nekim objektom.
        
        Args:
            obj: (Object) objekat koji potencijalno moze doci u koliziju sa sferom.
            
        Returns:
            True ili False u zavisnosit od toga da li je do kolizije doslo.
        """
        if type(obj) is BoundingSphere:
            #U slucaju da je objekat koji potencijalno moze doci u kolizju sa sferom takodje sfera,
            #vrsi se provera na osnovu rastojanja centara dve sfere.
            return sqrt((self.x() - obj.x())**2 + (self.y() - obj.y())**2 + (self.z() - obj.z())**2) <= self._r + obj._r
        elif type(obj) is BoundingPlane:
            #Ako je objekat koji potencijalno moze doci u koliziju sa sferom ravan (pravougaonik), provera kolizije se prepusta tom objektu.
            return obj.check_collision(self)
        

class BoundingPlane:
    def __init__(self, point_1 = [0, 0], point_2 = [0, 0], height = 0, inside = True):
        """
        Konstruktor klase BoundingPlane.
        
        Args:
            point_1: (list) koordinate gornjeg levog temena pravougaonika.
            point_2: (list) koordinate donjeg desnog temena pravougainika.
            height: (float) odredjuje na kojoj se visini pravougaonik nalazi (trenutno nema upotrebu).
            inside: (boolean) da li proveru treba vristi proveru unutrasnje kolizije ili spoljsanje.
        """
        self._point_1 = point_1
        self._point_2 = point_2
        self._height = height
        self._inside = inside
    
    def _outside_collision(self, obj):
        """
        Metoda koja vrsi proveru spoljasnje kolizije.
        
        Args:
            obj: (Object) objekat koji potencijalno moze doci u koliziju sa pravouganikom.
            
        Returns:
            True ili False u zavisnosit od toga da li je do kolizije doslo.
        """
        if type(obj) is BoundingSphere:
            #Ako je objekat koji potencijalno moze doci u koliziju sa pravouganikom sfera,
            #prvo se proverava kolizija izmedju sfere i stranica pravougaonika, u slucaju da ne
            #dolazi do kolizije sa stranicama pravouganika mora se dodatno proveriti da li je
            #doslo do kolizije sa temenima pravougaonika.
            return \
            ( ((self._point_1[0] <= obj.x()) and (self._point_2[0] >= obj.x())) and \
            (((self._point_1[1] <= obj.z() + obj._r) and (self._point_2[1] >= obj.z() + obj._r)) or \
            ((self._point_1[1] <= obj.z() - obj._r) and (self._point_2[1] >= obj.z() - obj._r))) ) \
            or \
            ( ((self._point_1[1] <= obj.z()) and (self._point_2[1] >= obj.z())) and \
               (((self._point_1[0] <= obj.x() + obj._r) and (self._point_2[0] >= obj.x() + obj._r)) or \
               ((self._point_1[0] <= obj.x() - obj._r) and (self._point_2[0] >= obj.x() - obj._r))) ) \
            or \
            ( (sqrt((self._point_1[0] - obj.x())**2 + (self._point_1[1] - obj.z())**2) <= obj._r) or \
            (sqrt((self._point_1[0] - obj.x())**2 + (self._point_2[1] - obj.z())**2) <= obj._r) or \
            (sqrt((self._point_2[0] - obj.x())**2 + (self._point_2[1] - obj.z())**2) <= obj._r) or \
            (sqrt((self._point_2[0] - obj.x())**2 + (self._point_1[1] - obj.z())**2) <= obj._r) )
    
    def _inside_collision(self, obj):
        """
        Metoda koja vrsi proveru unutrasnje kolizije.
        
        Args:
            obj: (Object) objekat koji potencijalno moze doci u koliziju sa pravouganikom.

        Returns:
            True ili False u zavisnosit od toga da li je do kolizije doslo.
        """
        if type(obj) is BoundingSphere:
            #objekat koji potencijalno moze doci u koliziju sa pravouganikom sfera, proverava se samo pozicija sfere
            #u odnosnu na stranice pravougaonika.
            return not ((obj.x() + obj._r < self._point_2[0]) and (obj.x() - obj._r > self._point_1[0]) and \
                        (obj.z() + obj._r < self._point_2[1]) and (obj.z() - obj._r > self._point_1[1]))
    
    def check_collision(self, obj):
        """
        Metoda koja proverava da li je doslo do kolizije sa nekim objektom.
        
        Args:
            obj: (Object) objekat koji potencijalno moze doci u koliziju sa pravouganikom.
            
        Returns:
            True ili False u zavisnosit od toga da li je do kolizije doslo.
        """
        if(self._inside == True):
            return self._inside_collision(obj)
        else:
            return self._outside_collision(obj)