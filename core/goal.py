#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

'''
Created on May 19, 2015

@author: Ivan Radosavljevic
'''

from PySide import QtGui
from core.bounds import BoundingSphere

class Goal:
    def __init__(self, world, mesh = None, position = [0, 0, 0]):
        self._world = world
        self._mesh = mesh
        self._bounds = BoundingSphere(position, 0.05)
        
        self._moving = False
        
    def _on_collision(self, obj):
        i = self._world._objects.index(obj)
        if (i <= 10) and (i != 0):
            self._world._objects[i].set_position([0, 0, 0])
        elif self._world._objects.index(obj) == 0:
            QtGui.QMessageBox.information(None, "Game over!", "White ball pocketed!")
            self._world._parent.reset_balls()
    
    def gather_graph_data(self):
        """
        Metoda koja sakuplja podatke potrebne za grafik brzine.
        U ovom slucaju ne radi nista, sto nema podatke o brzini kretanja.
        """
        pass
    
    def update(self):
        """
        Metoda koja azurira brzinu i poziciju stola.
        Sto ne menja ni brzinu ni poziciju, tako da je ova metoda prazna.
        """
        pass
    
    def draw(self):
        pass
        
    def check_collision(self, obj):
        """
        Metoda koja proverava da li je doslo do kolizije izmedju kugle i drugog objekta.
        
        Args:
            obj: (Object) objekat sa kojim se proverava da li je doslo do kolizije.
            
        Returns:
            True ako je doslo do kolizije, False ako nije doslo do kolizije.
        """
        if self._bounds.check_collision(obj._bounds):
            self._on_collision(obj)
            return True
        else:
            return False