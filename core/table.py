#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

'''
Created on Feb 28, 2015

@author: Ivan Radosavljevic
'''
from math import atan2, cos, sin
from core.bounds import BoundingPlane

class Table:
    def __init__(self, world, mesh = None, mass = 0.5, position = [0, 0, 0]):
        """
        Konstruktor klase Table.
        
        Args:
            world: (Object) model sveta u kom se vrsi simulacija.
            mesh: (Object) graficki model objekta.
            mass: (float) masa objekta.
            position: (list) x, y i z pozicija objekta.
        """
        self._world = world
        self._mesh = mesh
        self._moving = False #Status kretanja objekta.
        self._bounds = BoundingPlane([-1.26, -2.54], [1.26, 2.54]) #Model za proveru kolizije.
        self._mass = 1000
        self._cr = 0.5 #Koeficijent restitucije.
        
    def set_cr(self, cr):
        """
        Metoda koja podesava koeficijent restitucije.
        
        Args:
            cr: (float) koeficijent restitucje.
        """
        self._cr = cr
    
    def gather_graph_data(self):
        """
        Metoda koja sakuplja podatke potrebne za grafik brzine.
        U ovom slucaju ne radi nista, sto nema podatke o brzini kretanja.
        """
        pass
    
    def update(self):
        """
        Metoda koja azurira brzinu i poziciju stola.
        Sto ne menja ni brzinu ni poziciju, tako da je ova metoda prazna.
        """
        pass
        
    def draw(self):
        self._mesh.draw()
        
    def _on_collision(self, obj):
        if obj._moving == False:
            return
        
        directionx = 1
        directiony = 1
            
        #Da li je kugla udarila u levu ili desnu stranicu stola
        if ((obj._bounds.x() - obj._bounds._r) <= self._bounds._point_1[0]) or ((obj._bounds.x() + obj._bounds._r) >= self._bounds._point_2[0]):
            #Ako jeste vrati je na prethodnu poziciju.
            obj._bounds._position[0] -= self._world.integrate(self._world.dt, 0, obj._calculate_velocity_x)
            obj._bounds._position[2] -= self._world.integrate(self._world.dt, 0, obj._calculate_velocity_z)
                
            obj._mesh.set_position(obj._bounds._position[0], obj._bounds._position[1], obj._bounds._position[2])
                
            #Izracunaj tacno vreme kolizije.
            t = 0
            if(obj._velocity[0] != 0):
                t1 = (self._bounds._point_1[0] + obj._bounds._r - obj._bounds._position[0])/obj._velocity[0]
                t2 = (self._bounds._point_2[0] - obj._bounds._r - obj._bounds._position[0])/obj._velocity[0]
                t = max(t1, t2) - 0.0001
                #print(t)
                
            #Izracunaj tacnu poziciju na kojoj se kugla nalazila u datom trenutku (kada se kolizija desila).
            obj._bounds._position[0] += obj._velocity[0] * t
            obj._bounds._position[2] += obj._velocity[2] * t

            #Izmeni smer vektora brzine
            directionx = -1
                
        #Radi isto sto i prethodni kod samo za gornju i donju stranicu stola.
        if ((obj._bounds.z() - obj._bounds._r) <= self._bounds._point_1[1]) or ((obj._bounds.z() + obj._bounds._r) >= self._bounds._point_2[1]):                
            obj._bounds._position[0] -= self._world.integrate(self._world.dt, 0, obj._calculate_velocity_x)
            obj._bounds._position[2] -= self._world.integrate(self._world.dt, 0, obj._calculate_velocity_z)
                
            obj._mesh.set_position(obj._bounds._position[0], obj._bounds._position[1], obj._bounds._position[2])
                
            t = 0
            if(obj._velocity[0] != 0):
                t1 = (self._bounds._point_1[1] + obj._bounds._r - obj._bounds._position[2])/obj._velocity[2]
                t2 = (self._bounds._point_2[1] - obj._bounds._r - obj._bounds._position[2])/obj._velocity[2]
                t = max(t1, t2) - 0.0001
                
            obj._bounds._position[0] += obj._velocity[0] * t
            obj._bounds._position[2] += obj._velocity[2] * t
                
                
            directiony = -1
                
        #Izracunava intenzitet vektora brzine.
        #v = sqrt((self._velocity[0])**2 + (self._velocity[2])**2)

        #Racuna brzinu nakon sudara.
        obj._velocity_magnitude = obj._velocity_magnitude*(obj._mass - self._cr*self._mass)/(obj._mass + self._mass)
        obj._velocity_magnitude = abs(obj._velocity_magnitude)

        #Racuna novi smer kretanja.
        angle = atan2(obj._velocity[2], obj._velocity[0])

        obj._velocity[0] = (obj._velocity_magnitude * cos(angle)) * directionx
        obj._velocity[2] = (obj._velocity_magnitude * sin(angle)) * directiony
            
        obj._direction = [cos(angle) * directionx, 0, sin(angle) * directiony]
            
        #Postavlja novu poziciju grafickog modela kugle.
        obj._mesh.set_position(obj._bounds._position[0], obj._bounds._position[1], obj._bounds._position[2])
        
    def check_collision(self, obj):
        """
        Metoda koja proverava da li je doslo do kolizije izmedju kugle i drugog objekta.
        
        Args:
            obj: (Object) objekat sa kojim se proverava da li je doslo do kolizije.
            
        Returns:
            True ako je doslo do kolizije, False ako nije doslo do kolizije.
        """
        if self._bounds.check_collision(obj._bounds):
            self._on_collision(obj)
            return True
        else:
            return False