#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

'''
Created on 19.02.2015.

@author: Ivan Radosavljevic
         Aleksandra Mitrovic
'''
from core.helper import *

class IntegrationType:
    RK4 = 0
    EULER = 1

"""
Klasa za model sveta.
"""
class World:
    def __init__(self, parent = None):
        """
        Konstruktor objekta tipa World.
        """
        self.g = 9.81 #Gravitacija.
        self.dt = 0.01 #Vremenski korak.
        self._t = self.dt #Ukupno proteklo vreme simulacije.
        self.time =[] #Podaci o vremenu neophodni za grafik.
        self._simulating = False #Status simulacije.
        self._integration_type = IntegrationType.RK4 #Metoda koja se koristi za integraciju.
        self._objects = [] #Modeli Objekata koji postoje u svetu.
        self._parent = parent
    
    def add_object(self, obj):
        """
        Metoda koja dodaje model objekta u svet.
        
        Args:
            obj: (Object) objekat koji se dodaje u svet.
        """
        self._objects.append(obj)
    
    def set_integration_type(self, integration_type):
        """
        Metoda koja podesava koja ce metoda za integraciju biti upotrebljena.
        
        Args:
            integration_type: (enum) tip integracije.
        """
        self._integration_type = integration_type
    
    def integrate(self, x, y, f, dt = None):
        """
        Metoda koja vrsi integraciju odabranom metodom za integraciju.
        
        Returns:
            Vrednosti koje vrati metoda za integraciju.
        """
        if dt is None:
            dt = self.dt
        if self._integration_type == 0:
            return rk4(x, y, dt, f)
        else:
            return euler(x, y, dt, f)
    
    def update(self):
        """
        Metoda koja azurira stanja svih objekata u svetu.
        """
        self._simulating = False
        for i in range(len(self._objects)):
            if self._objects[i]._moving == True:
                #U slucaju da se bilo koji od objekata u svetu krece
                #simulaciju je neophodno nastaviti.
                self._simulating = True
                
            #Azurira objekte u svetu.
            self._objects[i].update()
            
        #Proverava kolizije izmedju objekata u svetu.
        for i in range(len(self._objects)-1):
            for j in range(i+1, len(self._objects)):
                if i != j:
                    self._objects[i].check_collision(self._objects[j])
                    

        #Crta objekte i sakuplja podatke neophodne za graf.
        for o in self._objects:
            o.gather_graph_data()
            o.draw()
            
        #Azurira vreme simulacije ako simulacija i dalje traje.
        if self._simulating:
            self.time.append(self._t)
            self._t += self.dt
        else:
            self._t = self.dt
