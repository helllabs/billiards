#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

'''
Created on Apr 2, 2015

@author: Ivan Radosavljevic
'''

def euler(t, y, dt, f):
    """
    Ojlerova metoda.
    """
    return y + dt*f(t, y)

def rk4(t, y, dt, f):
    """
    RK4 metoda.
    """
    k1 = f(t, y)
    k2 = f(t+dt/2, y + k1*dt/2)
    k3 = f(t+dt/2, y + k2*dt/2)
    k4 = f(t+dt, y + k3*dt)
    
    return y + (dt/6)*(k1 + 2*k2 + 2*k3 + k4)