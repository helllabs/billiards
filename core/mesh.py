#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

'''
Created on Feb 15, 2015

@author: Ivan Radosavljevic
'''
import numpy
import codecs
from OpenGL import GL
from PySide.QtGui import QImage

class Mesh:
    def __init__(self, device, file_path = "", texture_path = ""):
        self._device = device #Uredjaj koji vrsi iscrtavanje.
        self._vertices = [] #Temena mreze.
        self._normals = [] #Normale lica.
        self._uvmap = [] #UV koordinate za mapiranje teksture.
        if texture_path != "":
            self._texture = QImage(texture_path)
        else:
            self._texture = None
        self.x = 0
        self.y = 0
        self.z = 0
        self.x_rot = 0
        self.y_rot = 0
        self.z_rot = 0
        
        self.angle = 0
        
        self.load_mesh(file_path)
        
        #Moraju biti numpy nizovi! Liste su veoma spore!
        self._vertices = numpy.array(self._vertices)
        self._normals = numpy.array(self._normals)
        self._uvmap = numpy.array(self._uvmap)
        
    def set_position(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z
        
    def set_rotation(self, x, y, z):
        self.x_rot = x
        self.y_rot = y
        self.z_rot = z

    def draw(self):
        
        #Nalepi teksturu na objekat ako tekstura postoji.
        if self._texture is not None:
            GL.glBindTexture(GL.GL_TEXTURE_2D, self._device.bindTexture(self._texture))
            GL.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP)
            GL.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP)
            GL.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR)
            GL.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR)
        
        #Gurni matricu na stack.
        GL.glPushMatrix()
        #Obradi translaciju i rotaciju.
        GL.glTranslate(self.x, self.y , self.z)
        GL.glRotatef(self.angle, self.x_rot, self.y_rot, self.z_rot)
        #GL.glRotatef(self.y_rot, 0, 1, 0)
        #GL.glRotatef(self.z_rot, 0, 0, 1)
               
        #Blendovanje tekstura.
        GL.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA)
        
        GL.glNormalPointer(GL.GL_FLOAT, 0, self._normals)
        GL.glTexCoordPointer(2, GL.GL_FLOAT, 0, self._uvmap)
        GL.glVertexPointer(3, GL.GL_FLOAT, 0, self._vertices)
        
        
        GL.glEnableClientState(GL.GL_NORMAL_ARRAY)
        GL.glEnableClientState(GL.GL_TEXTURE_COORD_ARRAY)
        GL.glEnableClientState(GL.GL_VERTEX_ARRAY)
        
        GL.glDrawArrays(GL.GL_TRIANGLES, 0, len(self._vertices))

        GL.glDisableClientState(GL.GL_NORMAL_ARRAY)
        GL.glDisableClientState(GL.GL_TEXTURE_COORD_ARRAY)
        GL.glDisableClientState(GL.GL_VERTEX_ARRAY)

        #Ukloni teksture.
        if self._texture is not None:
            GL.glBindTexture(GL.GL_TEXTURE_2D, 0)
        
        GL.glPopMatrix()
        #Izbaci matricu sa stack-a.

    def load_mesh(self, file_path):
        self._vertices = []
        self._normals = []
        self._uvmap = []
        
        tmp_vert = []
        tmp_norm = []
        tmp_uvmp = []
        tmp_indice = []
        
        #Iscitaj i parsiraj datoteku.
        file = codecs.open(file_path, "r", encoding="UTF-8")
        for line in file:
            if line.split(" ", 1)[0] == "v":
                tmp_vert.append(self._read_values(line))
            elif line.split(" ", 1)[0] == "vt":
                tmp_uvmp.append(self._read_values(line))
            elif line.split(" ", 1)[0] == "vn":
                tmp_norm.append(self._read_values(line))
            elif line[0] == "f":
                tmp_indice.append(self._read_indices(line))
        file.close()
        
        #Smesti podatke u odgovarajuce liste.
        for i in range(len(tmp_indice)):
            for j in range(len(tmp_indice[i])):
                self._vertices.append(tmp_vert[tmp_indice[i][j][0]-1])
                self._uvmap.append(tmp_uvmp[tmp_indice[i][j][1]-1])
                self._normals.append(tmp_norm[tmp_indice[i][j][2]-1])
        
    def _read_values(self, line):
        values = line.split(" ", 1)[1].split()
        for i in range(min(3, len(values))):
            values[i] = float(values[i])
        return tuple(values)
    
    def _read_indices(self, line):
        values = line.split()
        for i in range(1, len(values)):
            values[i] = values[i].split("/")
            for j in range(len(values[i])):
                values[i][j] = int(values[i][j])
                
            values[i] = tuple(values[i])
                
        return tuple(values[1:])