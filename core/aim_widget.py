#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

'''
Created on 19.02.2015.

@author: Ivan Radosavljevic
         Aleksandra Mitrovic
'''
from PySide import QtGui, QtCore
from math import sqrt

class AimWidget(QtGui.QWidget):
    
    def __init__(self, parent = None):
        QtGui.QWidget.__init__(self, parent)
        self._pressed = False
        self.r = 0
        self.x_coordinate = None
        self.y_coordinate = None
        self.setMinimumHeight(100)
        self.setMinimumWidth(100)
        self.setSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)

    
    def paintEvent(self, event):
        
        self.r = min(self.width(), self.height())
        self.r -= 6
        gradient = QtGui.QRadialGradient(QtCore.QPointF(self.width() / 2, self.height() / 2), self.r)
        gradient.setColorAt(0.3, QtGui.QColor.fromRgb(243, 240, 225)) #centralna
        gradient.setColorAt(1, QtGui.QColor.fromRgb(160, 155, 130)) #boja oko ivica
        if (self.x_coordinate is None) or (self.y_coordinate is None):
            self.x_coordinate = self.width() / 2
            self.y_coordinate = self.height() / 2
        painter = QtGui.QPainter(self)
        painter.setPen(QtGui.QColor.fromRgb(182, 174, 130)) #boja linija (ivice)
        painter.setBrush(QtGui.QBrush(gradient))
        painter.drawEllipse(self.width() / 2 - self.r / 2, self.height() / 2 - self.r / 2, self.r, self.r) #kruznica
        
        #pokazivac mesta udaranja lopte
        painter.setBrush(QtGui.QBrush(QtGui.QColor.fromRgb(255, 0, 0, 100)))
        painter.drawEllipse(self.x_coordinate - 3, self.y_coordinate - 3, 6, 6)
        painter.setPen(QtGui.QColor.fromRgb(255, 0, 0))
        painter.drawPoint(self.x_coordinate, self.y_coordinate)
        painter.setPen(QtGui.QColor.fromRgb(182, 174, 130))
        painter.setBrush(QtGui.QBrush(QtCore.Qt.NoBrush))
        painter.drawEllipse(self.width() / 2 - 3, self.height() / 2 - 3, 6, 6)
        
    def mousePressEvent(self, event):
        if event.button() == QtCore.Qt.LeftButton:
            self._pressed = True
            self.mouseMoveEvent(event)
        
    def mouseMoveEvent(self, event):
        if self._pressed == True:
            if sqrt(((self.width() / 2) - event.x())**2 + ((self.height() / 2) - event.y())**2) <= self.r / 2:
                self.x_coordinate = event.x()
                self.y_coordinate = event.y()
                self.update()
                
    def mouseReleaseEvent(self, event):
        self._pressed = False
    
    def resizeEvent(self, event):
        if (self.x_coordinate is not None) and (self.y_coordinate is not None):
            self.x_coordinate += self.width() / 2 - event.oldSize().width() / 2
            self.y_coordinate += self.height() / 2 - event.oldSize().height() / 2
    
    def get_value(self):
        return self.x_coordinate, self.y_coordinate