#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

'''
Created on 19.02.2015.

@author: Ivan Radosavljevic
         Aleksandra Mitrovic
         
! velocity_magnitude - intenzitet vektora brzine treba smestiti kao polje
                       unutar klase ball jer se koristi na vise mesta i 
                       na svakom od njih ponovo racuna, dovoljno ga je
                       izracunati jednom u update() metodi.
! _on_collision() - Metoda _on_collision() treba da se napise citljivije.
'''
from core.bounds import BoundingSphere
from math import pi, atan2, cos, sin, sqrt

class Ball:
    
    def __init__(self, world, mesh = None, mass = 0.5, position = [0, 0, 0], force = 0):
        """
        Konstruktor klase Ball.
        
        Args:
            world: (Object) model sveta u kom se vrsi simulacija.
            mesh: (Object) graficki model objekta.
            mass: (float) masa objekta.
            position: (list) x, y i z pozicija objekta.
            force: (float) inicijalna sila koja deluje na objekat.
        """
        self._world = world
        self._mesh = mesh
        self._mass = mass
        self._force = force
        self._bounds = BoundingSphere(position, 0.05) #Model za proveru kolizije.
        self._direction = [0, 0, 0] #smer kretanja objekta.
        self._velocity = [0, 0, 0] #trenutna brzina objekta po x, y i z osi.
        self._friction = 0.2 #koeficijent trenja.
        self._moving = False #status kretanja objekta.
        
        self._velocity_magnitude = 0
        self._graph_data = [] #podaci za grafik brzine.
        
    def set_force(self, force):
        """
        Metoda koja podesava silu koja deluje na objekta.
        
        Args:
            force: (float) sila.
        """
        self._force = force
        
    def set_position(self, position):
        """
        Metoda koja postavlja novu vrednost pozicije objekta.
        
        Args:
            position: (list) nova pozicija po x, y i z osi.
        """
        self._bounds._position = position
        self._mesh.set_position(self._bounds._position[0], self._bounds._position[1], self._bounds._position[2])
        
    def _calculate_acceleration(self, t, v):
        """
        Metoda pomocu koje racunamo ubrzanje.
        
        Args:
            t: (float) trenutno vreme.
            v: (float) trenutna brzina.
            
        Returns:
            Izracunato ubrzanje.
        """
        if abs(v/t) + abs(self._force/self._mass) > abs(self._friction*9.81):
            return self._force/self._mass - self._friction*9.81
        else:
            return - self._force/self._mass - v/t
        
    def _calculate_velocity_x(self, t, x):
        """
        Metoda koja racuna brzinu po x osi.
        
        Args:
            t: (float) trenutno vreme.
            x: (float) trenutna pozicija po x osi.
            
        Returns:
            Izracunata brzina po x osi.
        """
        return self._velocity[0]
    
    def _calculate_velocity_z(self, t, z):
        """
        Metoda koja racuna brzinu po z osi.
        
        Args:
            t: (float) trenutno vreme.
            z: (float) trenutna pozicija po z osi.
            
        Returns:
            Izracunata brzina po z osi.
        """
        return self._velocity[2]
    
    def _calculate_angular_acceleration(self, t, v):
        """
        Metoda koja racuna ugaono ubrzanje.
        
        Args:
            t: (float) trenutno vreme.
            v: (float) trenutna brzina.
            
        Returns:
            Izracunato ugaono ubrzanje.
        """
        return (5/2)*(self._velocity_magnitude / t)*(self._bounds._r*7/5 - self._bounds._r)/(self._mass*self._bounds._r**2)
        
    def _calculate_angular_velocity(self, t, v):
        return v
        
    def reset_graph_data(self):
        """
        Metoda koja resetuje podatke za grafik brzine.
        """
        self._graph_data = []
        
    def gather_graph_data(self):
        """
        Metoda koja sakuplja podatke potrebne za grafik brzine.
        """
        if self._world._simulating:
            self._graph_data.append(self._velocity_magnitude)
        
    def update(self):
        """
        Metoda koja azurira brzinu i poziciju kugle.
        """
        
        self._velocity_magnitude = sqrt(self._velocity[0]**2 + self._velocity[2]**2) #Racuna intenzitet vektora brzine.
        self._velocity_magnitude = self._world.integrate(self._world.dt, self._velocity_magnitude, self._calculate_acceleration) #Vrsi integraciju i racuna novu vrednost intenziteta vektora brzine.
        
        
        #Racuna vrednosti komponenti vektora brzine.
        self._velocity[0] = self._velocity_magnitude * self._direction[0]
        self._velocity[2] = self._velocity_magnitude * self._direction[2]
        
        #Racuna novu poziciju kugle na osnovu brzine.
        self._bounds._position[0] = self._world.integrate(self._world.dt, self._bounds._position[0], self._calculate_velocity_x)
        self._bounds._position[2] = self._world.integrate(self._world.dt, self._bounds._position[2], self._calculate_velocity_z)
        
        
        #Proverava da li je brzina dovoljno velika da ima smisla vrsiti dalje proracune.
        if self._velocity_magnitude > 0.001:
            self._moving = True
        else:
            self._moving = False
            self._velocity[0] = 0
            self._velocity[2] = 0
            return
            
        #Postavlja poziciju grafickog modela kugle na novu vrednost.
        self._mesh.set_position(self._bounds._position[0], self._bounds._position[1], self._bounds._position[2])
            

        #Racuna ugao smera kretanja
        direction = atan2(self._velocity[2], self._velocity[0])
        direction =  pi - direction

        #Racuna vektor rotacije.
        rot_vec = [0, 0, 0]
        
        rot_vec[0] = self._direction[1] * 0 - sin(direction) * 1
        rot_vec[1] = sin(direction) * 0 - (-1)*cos(direction) * 0
        rot_vec[2] = (-1)*cos(direction) * 1 - self._direction[1] * 0

        #Racuna ugaonu brzinu
        angular_velocity = self._world.integrate(self._world.dt, 0, self._calculate_angular_acceleration)
        angle_dt = angular_velocity * self._world.dt #Treba prepraviti da koristi metod integrate!!!

        #Postavlja vrednosti vektora rotacije grafickog modela.
        self._mesh.x_rot = rot_vec[0]
        self._mesh.y_rot = rot_vec[1]
        self._mesh.z_rot = rot_vec[2]

        #Svodi ugao na vrednost od 0 do 360 stepeni.
        self._mesh.angle += angle_dt * (180 / pi)
        if self._mesh.angle >= 360:
            self._mesh.angle -= 360
        elif self._mesh.angle < 0:
            self._mesh.angle += 360
        
        #Sila deluje samo u jednom trenutku, nakon toga vraca se na vrednost 0.
        self._force = 0

        
    def draw(self):
        self._mesh.draw()
        
    def _on_collision(self, obj):
        """
        Metoda koja vrsi obradu kolizije sa drugim objektima.
        
        Args:
            obj: (Object) objekat sa kojim je kugla dosla u koliziju.
        """
        #U koliko je do kolizije doslo izmedju dve kugle, vraca pozicije kugli na prethodnu pozicju.
        obj._bounds._position[0] -= self._world.integrate(self._world.dt, 0, obj._calculate_velocity_x)
        obj._bounds._position[2] -= self._world.integrate(self._world.dt, 0, obj._calculate_velocity_z)
        
        self._bounds._position[0] -= self._world.integrate(self._world.dt, 0, self._calculate_velocity_x)
        self._bounds._position[2] -= self._world.integrate(self._world.dt, 0, self._calculate_velocity_z)
            
            
        #Racuna tacno vreme kolizije izmedju kugli.
        new_position = [self._bounds._position[0] - obj._bounds._position[0], self._bounds._position[2] - obj._bounds._position[2]]
        new_velocity = [self._velocity[0] - obj._velocity[0],
                        self._velocity[2] - obj._velocity[2]]
        r = obj._bounds._r + self._bounds._r
            
        discriminant = r**2 * (new_velocity[0]**2 + new_velocity[1]**2) - (new_position[0] * new_velocity[1] - new_position[1] * new_velocity[0])**2

        t = 0
        #Diskriminanta je pozitivna ako je do kolizije zaista doslo.
        if discriminant > 0:
            t1 = (-(new_position[0] * new_velocity[0] + new_position[1] * new_velocity[1]) + sqrt(discriminant)) / (new_velocity[0]**2 + new_velocity[1]**2)
            t2 = (-(new_position[0] * new_velocity[0] + new_position[1] * new_velocity[1]) - sqrt(discriminant)) / (new_velocity[0]**2 + new_velocity[1]**2)

            #Najmanje pozitivno vreme predstvalja vreme pkolizije.
            if (t1 < t2) and (t1 > 0):
                t = t1
            elif(t2 < t1) and (t2 > 0):
                t = t2

            #t -= 0.0001

            #Racuna tacnu poziciju kugli u trenutku kolizije.
            obj._bounds._position[0] += obj._velocity[0] * t
            obj._bounds._position[2] += obj._velocity[2] * t
                
            self._bounds._position[0] += self._velocity[0] * t
            self._bounds._position[2] += self._velocity[2] * t

            #Racuna ugao tangente na tacku kolizije.
            angle = atan2(self._bounds._position[2] - obj._bounds._position[2], self._bounds._position[0] - obj._bounds._position[0])
                
            #Racuna ugao kretanja kugli.
            first_ball_direction = atan2(obj._velocity[2], obj._velocity[0])
            second_ball_direction = atan2(self._velocity[2], self._velocity[0])
                
            #Racuna intenzitete brzina obe kugle.
            first_ball_magnitude = sqrt(obj._velocity[0]**2 + obj._velocity[2]**2)
            second_ball_magnitude = sqrt(self._velocity[0]**2 + self._velocity[2]**2)
                
            #Racuna nove brzine po x i z osi za obe kugle, po formuli za elasticni sudar.
            obj._velocity[0] = ((first_ball_magnitude * cos(first_ball_direction - angle) * (obj._mass - self._mass) \
                                        + 2*self._mass*second_ball_magnitude*cos(second_ball_direction - angle)) / (obj._mass + self._mass)) * cos(angle) \
                                        + first_ball_magnitude*sin(first_ball_direction - angle)*cos(angle + pi/2)
                                         
            obj._velocity[2] = ((first_ball_magnitude * cos(first_ball_direction - angle) * (obj._mass - self._mass) \
                                        + 2*self._mass*second_ball_magnitude*cos(second_ball_direction - angle)) / (obj._mass + self._mass)) * sin(angle) \
                                        + first_ball_magnitude*sin(first_ball_direction - angle)*sin(angle + pi/2) 
                                         
            self._velocity[0] = ((second_ball_magnitude * cos(second_ball_direction - angle) * (self._mass - obj._mass) \
                                        + 2*obj._mass*first_ball_magnitude*cos(first_ball_direction - angle)) / (self._mass + obj._mass)) * cos(angle) \
                                        + second_ball_magnitude*sin(second_ball_direction - angle)*cos(angle + pi/2) 
                                         
            self._velocity[2] = ((second_ball_magnitude * cos(second_ball_direction - angle) * (self._mass - obj._mass) \
                                        + 2*obj._mass*first_ball_magnitude*cos(first_ball_direction - angle)) / (self._mass + obj._mass)) * sin(angle) \
                                        + second_ball_magnitude*sin(second_ball_direction - angle)*sin(angle + pi/2)
                                         
                                         
            #Racuna nove smerove kretanja obe kugle.
            first_ball_direction = atan2(obj._velocity[2], obj._velocity[0])
            second_ball_direction = atan2(self._velocity[2], self._velocity[0])
                
            obj._direction = [cos(first_ball_direction), 0, sin(first_ball_direction)]
            self._direction = [cos(second_ball_direction), 0, sin(second_ball_direction)]

            #Postavlja nove pozicije za obe kugle.
            obj._mesh.set_position(obj._bounds._position[0], obj._bounds._position[1], obj._bounds._position[2])
            self._mesh.set_position(self._bounds._position[0], self._bounds._position[1], self._bounds._position[2])
                 
        
    def check_collision(self, obj):
        """
        Metoda koja proverava da li je doslo do kolizije izmedju kugle i drugog objekta.
        
        Args:
            obj: (Object) objekat sa kojim se proverava da li je doslo do kolizije.
            
        Returns:
            True ako je doslo do kolizije, False ako nije doslo do kolizije.
        """
        if self._bounds.check_collision(obj._bounds):
            obj._on_collision(self)
            return True
        else:
            return False
        
