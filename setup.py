from cx_Freeze import setup, Executable

# Dependencies are automatically detected, but it might need
# fine tuning.
buildOptions = dict(packages = ["Pyside", "OpenGL", "core", "numpy"], excludes = ["tk", "tkinter"])

import sys
base = 'Win32GUI' if sys.platform=='win32' else None
#base = 'console' if sys.platform=='win32' else None
executables = [
    Executable('main.py', base=base, targetName = 'Billiards.exe', icon='icon.ico')
]

setup(name='Billiards',
      version = '0.6',
      description = 'Billiards simulation',
      options = dict(build_exe = buildOptions),
      executables = executables)
