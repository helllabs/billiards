# Kratak opis #
Jednostavna simulacija bilijara kao projekat za predmet NANS.

# Uputstvo za upotrebu: #
Projekat je dostupan kao izvršiva datoteka ili u obliku izvornog koda. Izvršiv projekat je moguće skinuti sa linka: [https://bitbucket.org/helllabs/billiards/downloads/billiards.zip](https://bitbucket.org/helllabs/billiards/downloads/billiards.zip)

## Pokretanje projekta u izvršnom obliku: ##
Nakon što je izvršivi projekat preuzet potrebno je otvoriti Billiards.exe izvršivu datoteku kako bi se projekat pokrenuo.

## Pokretanje projekta iz izvornog koda: ##
Za pokretanje projekta iz izvornog koda i za dalju nadogradnju projekta neophodno je posedovati:

1. Python interpreter (verzija 3.4 ili novija)

1. NumPy modul za Python interpreter

1. PyOpenGL modul za Python interpreter

1. PySide modul za Python interpreter


### Instalacija Python interpretera: ###

Python interpreter se može preuzeti sa linka: [https://www.python.org/downloads/](https://www.python.org/downloads/).

Nakon preuzimanja interpretera neophodno je pokrenuti i izvršiti instalaciju interpretera.

### Instalacija NumPy modula: ###

NumPy modul se može instalirati unošenjem sledeće naredbe u komandnu liniju:

*python -m pip install -U NumPy*

### Instalacija PyOpenGL modula: ###

PyOpenGL modul se može instalirati unošenjem sledeće naredbe u komandnu liniju:

*python -m pip install -U PyOpenGL*

### Instalacija PySide modula: ###

PySide modul se može instalirati unošenjem sledeće naredbe u komandnu liniju:

*python -m pip install -U PySide*

# Autori: #
* Aleksandra Mitrović
* Ivan Radosavljević